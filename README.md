# Behavioral Cloning

This repository provides tools to collect demonstrations in `gym` environments and train CNN based policies based on the Behavioral Cloning algorithm.

## How to collect a dataset

To collect a dataset, run the command :

```
python -m bc.dataset.collect_demos with eval.env=<env_name> eval.dir=<dataset_dir>\
eval.seed=<init_seed> eval.episodes=<num_episodes> eval.workers=<num_processes>
```
If you want to collect a dataset of 200 demonstrations of the environment `UR5-PickCamEnv-v0` using 10 processes then run:
```
python -m bc.dataset.collect_demos with eval.env=UR5-PickCamEnv-v0 eval.dir=/home/user/pick_dataset\
eval.seed=0 eval.episodes=200 eval.workers=10
```

You can store images in 2 different modes, set `eval.db_type` as :
* `lmdb` : demonstrations are stored in an LMDB dataset, each key a the database if of the form `demo/step` and contains all the images of the environment of the current timestep
* `gif` : for visualization, store each demonstration in a `gif`. For this feature, `skvideo` needs to be configured in `utils/videos.py` by setting ffmpeg path

If you have `dask` available, you can use it by adding the flag `eval.dask=True`, the default data collection is parallelized on one machine using `joblib`.

## How to train a model

Once you have collected a dataset of demonstrations, you can train a model on it. Run the command:

```
python -m bc.net.train with eval.dataset.dir=<dataset_dir> model.dir=<checkpoints_dir>
```

This command starts training with the default set of parameters defined in `bc/config.py`.
You can every parameters directly in command-line. Here is a short description of the parameters :

* `model.dir`: directory to save the checkpoints during training
* `model.archi`: net architecture
* `model.num_frames`: number of frames stacked for network input
* `model.channels`: channels used for input, either `depth`, `rgb` or `rgbd`
* `model.action_space`: either `joints` or `tool`
* `model.steps_action`: steps of actions to predict in the future
* `model.skills`: always equal to `1` for vanilla bc, define number of skills\\

* `dataset.dir`: directory of the dataset to train on
* `dataset.max_demos`: set the number of demonstrations to train on
* `dataset.num_cameras`: set the number of cameras to train on\\

* `train.lam_grip`: scaling parameter for gripper cross-entropy loss
* `train.workers`: number of processes to use for data loading
* `train.eval_proportion`: proportion of the dataset witheld for evaluation

During training, you can check the loss and validation curves using `tensorboard`, run:
```
tensorboard --logdir <checkpoints_dir>
```

To run a configuration of your own, you can create a json file containing a dictionnary
`config.json` with all your set of parameters and run the command:

```
python -m bc.net.train with config.json
```

## How to evaluate a model

Once you trained an agent, you can test checkpoints with the following command:

```
python -m bc.dataset.collect_demos with eval.env=<env_name> eval.dir=<report_dir>
model.dir=<model_dir> eval.net_epoch=<net_epoch> eval.seed=<init_seed> eval.episodes=<num_episodes> 
eval.db_type=report eval.workers=<num_processes>
```

It will run the model on the chosen seeds and report the success in a `.rep` report file that can be
loaded afterwards with the `Report` class of `bc.utils`.

## How to run tests

```
python -m bc.tests.run
```

## Environment requirements

The observable of your `gym` environment should de stored in a `state` key, if you observe the environment using images, then they should be stored with the keys `rgb{}`, `depth{}` or `mask{}` depending on their type where `{}` is the label of the camera your observe the scene from.
