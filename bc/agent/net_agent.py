import os
import click
import numpy as np
from collections import OrderedDict
import cv2
import json

import torch
import torch.nn as nn
from torchvision import transforms
from PIL import Image

from .agent import Agent
from ..net import Network, NetworkHierarchical, NetworkDA, NetworkGoal,\
    NetworkReg, NetworkSkills
from bc.dataset import Frames
#from utils_perso.videos import write_video


class NetAgent:
    def __init__(self, archi, channels, path, num_frames, skip, max_steps, epoch=-1,
                 action_space='tool', dim_action=4, steps_action=1, num_skills=1,
                 device=None):

        if channels == 'rgbd':
            self._channels = ['rgb', 'depth']
        else:
            self._channels = [channels]
        self._name_channels = channels
        num_channels = {'depth': 1, 'rgb': 3, 'rgbd': 4}
        self._num_channels = num_channels[channels]
        self._num_frames = num_frames
        self._skip = skip
        self._action_space = action_space

        # multi GPU
        num_gpu = torch.cuda.device_count()
        # print('GPUs', num_gpu)

        # load snapshot
        if epoch == -1:
            suffix = 'current'
        else:
            suffix = str(epoch)

        net_path = os.path.join(path, '{}_{}.pth'.format(archi, suffix))
        infos_path = os.path.join(path, 'info.json')

        # action stored as gripper_vel, lin_vel, ang_vel
        if num_skills > 1:
            self.net = NetworkSkills(
                archi=archi, input_type=channels, timesteps=num_frames,
                action_space=action_space, dim_action=dim_action,
                steps_action=steps_action, num_subpolicies=num_skills,
                path=net_path, device=device)
        else:
            self.net = Network(
                archi=archi, input_type=channels, timesteps=num_frames,
                action_space=action_space, dim_action=dim_action,
                steps_action=steps_action, path=net_path, device=device)
        self.net.set_eval()


        # define action space
        if action_space == 'tool':
            self.keys_action = ['linear_velocity']
            if dim_action == 7:
                self.keys_action += ['angular_velocity']
        elif action_space == 'joints':
            self.keys_action = ['joint_velocity']
        else:
            raise ValueError('Unkown action space : {}'.format(action_space))

        # get network dataset statistics
        stats = json.load(open(infos_path, 'r'))['statistics']
        mean, std = {}, {}
        if 'grip_state' in stats['state']:
            mean['grip_velocity'], std['grip_velocity'] = stats['state']['grip_state']
        elif 'grip_velocity' in stats['state']:
            mean['grip_velocity'], std['grip_velocity'] = stats['state']['grip_velocity']

        for key_action in self.keys_action:
            mean[key_action], std[key_action] = stats['action'][key_action]
        self.mean = mean
        self.std = std

        # queues for image and signal history
        self._hist_frames = None
        self._hist_sig = None

        self._count_steps = 0
        self._max_steps = max_steps

    def get_action(self, obs, skill=None):
        self._count_steps += 1
        if self._max_steps > -1 and self._count_steps > self._max_steps:
            return None

        channels = self._channels
        num_channels = self._num_channels

        for key, value in obs.items():
            for channel in channels:
                if channel in key:
                    obs[channel] = obs.pop(key)
        frames = Frames.transform_frames([obs], channels, num_channels)

        # queue of observed images
        hist_frames = self._hist_frames
        if self._hist_frames is None:
            hist_frames = Frames.adjust_shape(frames, self._num_frames, self._name_channels)
        else:
            # shift past images to the left
            if self._num_frames > 1:
                hist_frames[:-num_channels, :, :] = hist_frames[num_channels:, :, :]
            hist_frames[-num_channels:] = frames

        self._hist_frames = hist_frames

        net_frames = hist_frames[::self._skip].float()
        dic_action = self.net.get_dic_action({'frames': net_frames[None, :]}, skill)
        for key_action in dic_action:
            if key_action != 'grip_velocity':
                dic_action[key_action] = self.mean[key_action]+\
                                         dic_action[key_action]*self.std[key_action]
        dic_action['grip_velocity'] *= 2

        return dic_action
