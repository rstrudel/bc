import torch
from torch.utils.data import Dataset

from . import Frames, Actions

class ImitationDataset(Dataset):

    def __init__(self, dir, max_demos, num_cameras,
                 num_frames, channels, action_space, dim_action,
                 steps_action, num_skills):

        # define frames dataset
        frames = Frames(dir, channels, mode='pytorch')
        frames.keys.set_query_limit('demo')
        frames.keys.set_max_demos(max_demos)

        # define actions dataset
        actions = Actions(dir, action_space, dim_action)
        self._get_skills = num_skills > 1
        if self._get_skills:
            actions.keys.set_query_limit('skill')
        else:
            actions.keys.set_query_limit('demo')
        actions.keys.set_max_demos(max_demos)

        assert len(frames) == len(actions),\
                'Frames length {} Actions length {}'.format(len(frames), len(actions))

        self._frames = frames
        self._actions = actions
        self._channels = channels
        self._num_frames = num_frames
        self._num_cameras = num_cameras
        self._steps_action = steps_action
        self._action_space = action_space
        self._dim_action = dim_action

    def __len__(self):
        return len(self._frames)

    def __getitem__(self, idx):
        frames = self._frames
        actions = self._actions
        channels = self._channels
        num_frames = self._num_frames
        steps_action = self._steps_action

        # sample camera
        cam_num = int(torch.randint(high=self._num_cameras, size=(1,)))
        frames.set_cam_num(cam_num)

        # images
        frames.keys.set_idx_reference(idx)
        idx_beg, idx_end = self.get_bounded_idxs(idx-(num_frames-1), idx+1)
        im = frames[idx_beg:idx_end]
        im = frames.adjust_shape(im, num_frames, channels)

        # actions
        actions.keys.set_idx_reference(idx)
        idx_beg, idx_end = self.get_bounded_idxs(idx+steps_action[0], idx+steps_action[-1]+1)
        actions_future = actions[idx_beg:idx_end]
        action = actions.adjust_shape(actions_future, steps_action)
        skill = actions.get_skill(idx, skip_undefined=True)

        return (im.float(),
                action,
                skill,
                torch.zeros(1).long())

    def get_bounded_idxs(self, idx_min, idx_max):
        return min(max(idx_min, 0), len(self)-1), max(min(idx_max, len(self)), 0)

    def get_statistics(self):
        return self._actions.get_statistics()
