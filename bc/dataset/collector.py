import os
import gym
import numpy as np
import json
import click
import shutil
import pickle as pkl

from bc.dataset import DatasetWriter
from bc.dataset.utils import compress_images, process_trajectory
from . import utils

try:
    from bc.utils import videos
except Exception as _:
    pass

from mime.agent import ScriptAgent
from ..agent import NetAgent


class Collector:
    def __init__(
            self, env, agent_type, dataset_path, net_path, max_steps,
            timescale, skill_sequence, record_failed=False, db_type='lmdb',
            num_steps_buffer=100, plugin='', render=False):
        self.env_name = env
        self.agent_type = agent_type
        self.path_dataset = dataset_path
        self.net_path = net_path
        self.dataset_type = db_type
        self.record_fails = record_failed
        self.num_steps_buffer = num_steps_buffer
        self.max_steps = max_steps
        self.plugin = plugin
        self.render = render
        self.timescale = timescale
        self.skill_sequence = skill_sequence

        if self.dataset_type not in ['lmdb', 'gif', 'report']:
            raise ValueError('Unknown dataset type : {}'.format(self.dataset_type))

        if not os.path.isdir(self.path_dataset):
            os.mkdir(self.path_dataset)

        # create the environment
        self.env = gym.make(self.env_name)
        if self.render:
            self.env.unwrapped.scene.renders(True)

    def __call__(self, seed):
        success, seed, epoch, count = self.run_episode(seed)

        return success, seed, epoch, count

    def run_episode(self, seed_epoch):
        seed, epoch = seed_epoch
        agent_type = self.agent_type
        dataset_type = self.dataset_type
        timescale = self.timescale
        skill_sequence = self.skill_sequence

        if dataset_type == 'lmdb':
            path_worker_dataset = os.path.join(self.path_dataset, '{:06}'.format(seed))
            dataset = DatasetWriter(path_worker_dataset, self.env_name, rewrite=True,
                                    float_depth=False)
            dataset.init_db()

        observs, actions, steps = [], [], []

        self.env.seed(seed)
        # plugin to change the environment after reset
        if self.plugin == 'skill':
            self.plugin_skill(self.env)
        obs = self.env.reset()

        # gripper max vel, lin max vel, ang max vel
        info_dataset = os.path.join(self.path_dataset, 'info.json')
        json.dump(dict(env_name=self.env_name), open(info_dataset, 'w'))

        if agent_type == 'script':
            agent = ScriptAgent(self.env)
        elif agent_type == 'net':
            # action_factors = np.array([2.6, 0.1, 0.1, 0.1, 0.5, 0.5, 0.5])
            infos = json.load(open(os.path.join(self.net_path, 'info.json'), 'r'))
            agent_info = {'path': self.net_path,
                          'epoch': epoch,
                          'num_skills': 1,
                          'max_steps': self.max_steps,
                          'skip': 1}
            filter_infos = ['archi', 'channels', 'num_frames',
                            'action_space', 'dim_action', 'steps_action',
                            'num_skills']
            if 'skip' not in infos:
                infos['skip'] = 1
            for info in filter_infos:
                agent_info[info] = infos[info]
            agent_info['steps_action'] = len(agent_info['steps_action'])
            agent = NetAgent(**agent_info)

        skill = skill_sequence[0]
        if agent_type == 'net':
            act = agent.get_action(obs, skill)
        elif agent_type == 'script':
            act = agent.get_action()
        done = False
        count = 0
        count_master = 0
        scalars = {}

        while not act is None:
            if agent_type == 'net' and done:
                break
            if dataset_type == 'lmdb':
                compress_images(obs)
            if dataset_type != 'report':
                observs.append(obs)
                actions.append(act)
                steps.append(count)
            count += 1

            obs, reward, done, info = self.env.step(act)
            if done and len(info['failure_message']) > 0:
                break

            if timescale > 0 and count%timescale == 0:
                count_master += 1
            if count_master == len(skill_sequence):
                break
            skill = skill_sequence[count_master]
            if agent_type == 'net':
                act = agent.get_action(obs, skill)
            elif agent_type == 'script':
                act = agent.get_action()
            # if more than num_steps_buffer steps are performed,
            # write in the dataset and clear buffer
            if dataset_type == 'lmdb' and count%self.num_steps_buffer == 0:
                frames_chunk, scalars_chunk = process_trajectory(
                    observs, actions, steps, seed, jpeg_compression=False)
                dataset.write_frames(frames_chunk)
                scalars.update(scalars_chunk)
                pkl.dump(scalars,
                    open(os.path.join(self.path_dataset, '{:06}.pkl'.format(seed)), 'wb'))
                observs, actions, steps = [], [], []

        # write residual chunk to dataset if success
        # remove dataset if filtering failures
        if dataset_type == 'lmdb':
            print('success', info['success'])
            if observs and\
            (info['success'] or (not info['success'] and self.record_fails)):
                print('Writing trajectory {}'.format(seed))
                frames, scalars_chunk = process_trajectory(
                    observs, actions, steps, seed, jpeg_compression=False)
                dataset.write_frames(frames)
                dataset.close_db()
                scalars.update(scalars_chunk)
                pkl.dump(scalars,
                    open(os.path.join(self.path_dataset, '{:06}.pkl'.format(seed)), 'wb'))
            elif not info['success'] and not self.record_fails:
                dataset.close_db()
                shutil.rmtree(path_worker_dataset)
        elif dataset_type == 'gif':
            skip = 2
            path_video = os.path.join(self.path_dataset, '{:06}.gif'.format(seed))
            videos.write_video([obs['rgb0'] for obs in observs[::skip]], path_video)

        if not info['success']:
            if act is None:
                info['failure_message'] = 'Reached max steps.'
            click.secho('Failure Seed {}: {}'.format(seed, info['failure_message']), fg='red')

        return info['success'], seed, epoch, count

    # plugin for collecting skills in BowlEnv
    def plugin_skill(self, env):
        env.unwrapped.scene.skill_data_collection = True
