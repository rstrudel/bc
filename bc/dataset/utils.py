from PIL import Image
import lmdb
import pickle as pkl
import struct
from io import BytesIO
import os
from tqdm import tqdm
import shutil
import re
import copy
import torch
from torch.utils.data import Dataset

from bc.dataset import Scalars, Keys
from bc.utils import Report

class Subset(Dataset):
    """
    Subset of a dataset at specified indices.
    Arguments:
        dataset (Dataset): The whole Dataset
        indices (sequence): Indices in the whole set selected for subset
    """
    def __init__(self, dataset, indices):
        self.dataset = dataset
        self.indices = indices

    def __getitem__(self, idx):
        return self.dataset[self.indices[idx]]

    def __len__(self):
        return len(self.indices)

def create_report(report_dir, net_path, env_name):
        dir_idx = net_path.rfind('/')
        net_dir = net_path[:dir_idx]
        net_name = net_path[dir_idx+1:]
        report_path = os.path.join('{}/{}.rep'.format(report_dir, net_name))
        if os.path.exists(report_path):
            print('Report {} already exists.'.format(report_path))
            report = Report(path=report_path)
        else:
            report = Report(env_name, net_path)

        return report, report_path

def gather_dataset(dataset_path):
    scalars = {}
    # gather trajectory datasets in one dataset
    list_trajs = os.listdir(dataset_path)
    dataset = lmdb.open(dataset_path, 200*1024**3, writemap=True)
    keys_dataset = []
    with dataset.begin(write=True) as txn_w:
        for traj in tqdm(list_trajs):
            path_traj = os.path.join(dataset_path, traj)
            if 'pkl' in path_traj:
                traj_scalars = pkl.load(open(path_traj, 'rb'))
                scalars.update(traj_scalars)
                os.remove(path_traj)
            elif os.path.isdir(path_traj)\
                 and os.path.exists(os.path.join(path_traj, 'data.mdb')):
                db_traj = lmdb.open(path_traj, readonly=True)
                with db_traj.begin(write=False) as txn_r:
                    for key, value in txn_r.cursor():
                        keys_dataset.append(key.decode('ascii'))
                        txn_w.put(key, value)
                db_traj.close()
                shutil.rmtree(path_traj)

    # save dataset list of keys
    keys_file = os.path.join(dataset_path, '_keys_')
    keys = Keys(keys=keys_dataset)
    pkl.dump(keys, open(keys_file, 'wb'))

    path_scalars = os.path.join(dataset_path, 'scalars.pkl')
    scalars = Scalars(scalars)
    pkl.dump(scalars, open(path_scalars, 'wb'))

# store jpeg compressed image as bytes format
# mask stays untouched, JPEG compression alterates mask
def compress_images(obs):
    im_keys = [k for k in obs.keys() if 'rgb' in k or 'depth' in k]
    for im_key in im_keys:
        im = Image.fromarray(obs[im_key])
        im_buf = BytesIO()
        im.save(im_buf, format='JPEG', quality=100)
        obs[im_key] = im_buf.getvalue()

def process_trajectory(states, actions, steps, seed, jpeg_compression=True):
    assert len(states) == len(actions), 'Len of actions and states not the same'
    if not states:
        return

    chunk_trajectory = {}
    s = states[0]

    # TODO : remove regex
    # regex to extract camera number
    # regex_num = re.compile('[^0-9]')
    # # # regex to extract channel
    # regex_channel = re.compile('[^a-zA-Z]')

    im_keys = ['rgb', 'depth', 'mask']
    frames = {}
    scalars = {}

    for i, (state, action, step) in enumerate(zip(states, actions, steps)):
        data_im_keys = [k for k in state.keys() if any(im_key in k for im_key in im_keys)]
        # tot_cam = max([int(regex_num.sub('', k)) for k in data_im_keys])
        dic_buf = BytesIO()

        # unique key for (seed, cam, step) triplet

        # format and store images
        # store jpeg compressed image as bytes format
        for im_key in data_im_keys:
            # channel = regex_channel.sub('', im_key)
            # num_cam = int(regex_num.sub('', im_key))
            if (seed, step) not in frames:
                frames[(seed, step)] = {}
            if jpeg_compression:
                im = Image.fromarray(state[im_key])
                im_buf = BytesIO()
                im.save(im_buf, format='JPEG', quality=100)
                frames[(seed, step)][im_key] = im_buf.getvalue()
            else:
                frames[(seed, step)][im_key] = state[im_key]
            state.pop(im_key)

        # store scalar values
        scalars[(seed, step)] = {'action': action, 'state': state}

    return frames, scalars
