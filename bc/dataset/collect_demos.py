import click
import time
import os
from tqdm import tqdm
import lmdb
import shutil
import json
from termcolor import colored
from sacred import Experiment
from sacred.observers import FileStorageObserver

from dask.distributed import Client, LocalCluster, progress
from joblib import Parallel, delayed

try:
    from daskoia import CPUCluster
except Exception as _:
    pass

from ..config import eval_ingredient
from .collector import Collector
from .utils import create_report, gather_dataset

ex = Experiment('eval', ingredients=[eval_ingredient])
# ex.observers.append(FileStorageObserver.create('/sequoia/data1/rstrudel/experiments/bc'))

@ex.capture
def make_collector(model, eval):
    agent_type = 'net' if model['dir'] else 'script'
    plugin = 'skill' if eval['skill_collection'] else ''
    collector = Collector(eval['env'], agent_type, eval['dir'], model['dir'], eval['max_steps'],
                          eval['timescale'], eval['skill_sequence'], eval['record_failed'],
                          eval['db_type'], plugin=plugin, render=eval['render'])
    seed_range = range(eval['seed'], eval['seed'] + eval['episodes'])
    seed_epoch = []
    report, report_path = None, None
    if eval['db_type'] == 'report':
        report, report_path = create_report(eval['dir'], model['dir'], eval['env'])

        for epoch in range(eval['first_epoch'], eval['last_epoch'], eval['iter_epoch']):
            for seed in seed_range:
                if not report.is_entry(epoch, seed):
                    seed_epoch.append((seed, epoch))
    else:
        seed_epoch = [(seed, eval['net_epoch']) for seed in seed_range]

    return collector, seed_epoch, report, report_path, eval['dir']

@ex.capture
def run_parallel(collector, seed_epoch, eval):
    if eval['dask']:
        # Launching Dask on cluster
        assert not eval['render'], 'Can not render using dask'
        cluster = CPUCluster(mem_req=4000)
        cluster.start_workers(eval['workers'])
        client = Client(cluster)
        print('Scheduler Info {}'.format(client.scheduler_info()))
        futures_traj = client.map(collector, seed_epoch)
        progress(futures_traj)
        results = client.gather(futures_traj)
    else:
        if eval['workers'] > 1:
            assert not eval['render'], 'Can not render using multiple processes'
            results = Parallel(n_jobs=config.processes)(
                delayed(collector)(se) for se in tqdm(seed_epoch))
        else:
            assert eval['workers'] == 1, 'Number of processes should be 1'
            results = (collector(se) for se in tqdm(seed_epoch))

    return results

@ex.capture
def process_results(results, report, report_path, eval):
    episodes, failed_episodes = eval['episodes'], []
    tot_steps = 0
    max_steps = 0
    for success, seed, epoch, num_steps in results:
        if eval['db_type'] == 'report':
            report.add_entry(seed, epoch, success)
        if success or eval['record_failed']:
            tot_steps += num_steps
            if num_steps > max_steps:
                max_steps = num_steps
        else:
            failed_episodes += [seed, ]
            episodes -= 1

    if eval['db_type'] == 'lmdb':
        print('Gathering trajectories dataset into one dataset...')
        gather_dataset(eval['dir'])
    elif eval['db_type'] == 'report':
        report.save(report_path)

    return episodes, failed_episodes, tot_steps, max_steps

@ex.automain
def main():
    t0 = time.time()

    collector, seed_epoch, report, report_path, data_dir = make_collector()
    results = run_parallel(collector, seed_epoch)
    episodes, failed_episodes, tot_steps, max_steps = process_results(results, report, report_path)

    print(colored('Dataset successfully written to {}'.format(data_dir), 'green'))
    print(colored('Total {} steps in {} trajectories'.format(tot_steps, episodes), 'green'))
    print(colored('Maximum {} steps in one trajectory'.format(max_steps), 'green'))
    print(colored('Data collection took {} seconds'.format(time.time()-t0), 'green'))
    if len(failed_episodes):
        print(colored('Failed {} trajectories: {}'.format(len(failed_episodes),
                                                        sorted(failed_episodes)), 'red'))
