import torch
from torchvision import transforms
import numpy as np

from .dataset_lmdb import DatasetReader
from PIL import Image


class Frames:
    def __init__(self, path, channels, limit='', mode='numpy'):
        self._db = DatasetReader(path, channels)
        self.keys = self._db.keys
        self._limit = limit
        self.keys.set_query_limit(limit)
        assert mode in ['numpy', 'pytorch']
        self._mode = mode

        if channels == 'rgbd':
            self.channels = ['rgb', 'depth']
        else:
            self.channels = [channels]
        num_channels = {'depth':1, 'rgb': 3, 'rgbd': 4}
        self.num_channels = num_channels[channels]

    def __len__(self):
        return len(self._db)

    def __getitem__(self, idx):
        db = self._db
        keys = self.keys
        if isinstance(idx, slice):
            start, end, step = idx.indices(len(self))
            # step bigger than 1 not handled
            assert step == 1
            # make sure all the frames come from the same demo
            idx_min, idx_max = keys.get_idx_min_max(start, end)
            frames = db[idx_min:idx_max]
        elif isinstance(idx, int):
            frames = db[idx]
        else:
            raise TypeError('{} is an unvalid index type.'.format(type(idx)))

        if self._mode == 'pytorch':
            frames = self.transform_frames(frames, self.channels, self.num_channels)
        # for key, value in self._avg_time.items():
        #     print(key, sum(value)/len(value))

        return frames

    @staticmethod
    def transform_frames(frames, channels, num_channels):
        tot_channels = len(frames)*num_channels
        transform = transforms.Compose([
            transforms.Normalize((0.5, )*tot_channels, (0.5, )*tot_channels,)])

        stack_frames = np.zeros((0, 224, 224))
        for frame in frames:
            for channel in channels:
                unit_frame = frame[channel]/255
                if channel == 'rgb':
                    unit_frame = unit_frame.T
                elif channel == 'depth':
                    unit_frame = unit_frame[None, :]
                stack_frames = np.vstack((stack_frames, unit_frame))
        assert tot_channels == stack_frames.shape[0]
        stack_frames = torch.tensor(stack_frames)
        tensor_frames = transform(stack_frames)
        return tensor_frames

    @staticmethod
    def adjust_shape(x, timesteps, channels):
        num_channels = {'depth': 1, 'rgb': 3, 'rgbd': 4}
        channels = num_channels[channels]

        x_chan = x.shape[0]
        assert x_chan%channels == 0
        if x_chan != timesteps*channels:
            missing_frames = int(timesteps-x_chan/channels)
            m = x[:channels].repeat(missing_frames, 1, 1)
            x = torch.cat((m, x), dim=0)

        return x

    def set_cam_num(self, cam_num):
        self._db.set_cam_num(cam_num)

    def set_max_demos(self, max_demos):
        self._db.set_max_demos(max_demos)
