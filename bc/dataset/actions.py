import os
import pickle as pkl
import numpy as np
import torch

from .scalars import Scalars

class Actions:
    def __init__(self, path, action_space, dim_action):
        path_scalars = os.path.join(path, 'scalars.pkl')
        self._scalars = pkl.load(open(path_scalars, 'rb'))
        self.keys = self._scalars.keys
        self._stats = self._scalars.get_statistics()
        self.action_space = action_space
        self.dim_action = dim_action
        self._skills = None

        if action_space == 'tool':
            if self.dim_action == 4:
                self.keys_action =   ['grip_velocity', 'linear_velocity']
            elif self.dim_action == 7:
                self.keys_action = ['grip_velocity', 'linear_velocity', 'angular_velocity']
        elif action_space == 'joints':
            assert self.dim_action == 7
            self.keys_action = ['grip_velocity', 'joint_velocity']
        else:
            raise ValueError('Unknown action space : {}'.format(action_space))

        if 'skill' in self._scalars[0]['state']:
            self._load_skills()

    def __len__(self):
        return len(self._scalars)

    def __getitem__(self, idx):
        if isinstance(idx, int) or isinstance(idx, np.int64):
            action_scalars = [self._scalars[int(idx)]]
        elif isinstance(idx, slice):
            action_scalars = self._scalars[idx]
        else:
            raise TypeError('{} is an unvalid index type. {}'.format(type(idx), idx))
        stack_actions = [self.normalize(asc, self.dim_action, self.keys_action, self._stats)\
                         for asc in action_scalars]
        actions = np.vstack(stack_actions)

        return actions

    def _load_skills(self):
        scalars = self._scalars
        self._skills = [scalar['state']['skill'] for scalar in scalars]

    @staticmethod
    def normalize(action_scalar, dim_action, keys_action, stats):
        state = action_scalar['state']
        action = action_scalar['action']

        idx_action = 0
        norm_action = np.zeros(dim_action)

        # use the grip_velocity of the state which is 1 when open, -1 when closed
        # the grip_velocity of action is -1 only when the gripper is closing, but not when closed
        grip_obs = ['grip_state', 'grip_velocity']
        grip_in_state = False
        for g_obs in grip_obs:
            if g_obs in state:
                grip_in_state = True
                norm_action[idx_action] = state[g_obs]
                idx_action += 1

        if grip_in_state:
            keys_actions = keys_action[1:]
        else:
            keys_actions = keys_action

        for key_action in keys_actions:
            mean, std = stats['action'][key_action]
            size = mean.shape
            if not size:
                size = (1,)
            raw_action = action[key_action]
            if 'grip' in  key_action:
                unit_action = raw_action
            else:
                unit_action = (raw_action-mean)/std
            norm_action[idx_action:idx_action+size[0]] = unit_action
            idx_action += size[0]

        return norm_action

    @staticmethod
    def adjust_shape(actions, steps_action):
        action = torch.zeros(0).float()
        for step_action in steps_action:
            idx_a = min(step_action-steps_action[0], len(actions)-1)
            a = torch.tensor(actions[idx_a]).float()
            action = torch.cat((action, a), 0)
        return action

    def get_skill(self, idx, skip_undefined=False):
        a = torch.zeros(1).long()
        if self._skills is not None:
            a = self._skills[idx]
        if a < 0 and skip_undefined:
            a = self._skills[idx+1]
        return a

    def get_statistics(self):
        return self._stats

    def set_max_demos(self, max_demos):
        self._scalars.set_max_demos(max_demos)
