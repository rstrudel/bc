import os
import subprocess
import argparse
import glob
import shutil
from termcolor import colored

HOME = os.environ['HOME']
parser = argparse.ArgumentParser()

parser.add_argument('--logdir', type=str, default='{}/Data/unittest'.format(HOME),
                    help='directory where to save the logs of the tests')
args = parser.parse_args()

UNITTESTS = [
    ['data collection on dask',
     'python -m bc.dataset.collect_demos --env=UR5-BowlCamEnv-v0 --dataset_path={}/test1 -ms 10 -e 2 -n 2 --db_type=lmdb --dask --skill-collection --record-failed'.format(args.logdir)],
    ['data collection on joblib',
     'python -m bc.dataset.collect_demos --env=UR5-BowlCamEnv-v0 --dataset_path={}/test2 -ms 10 -e 2 -n 2 --db_type=lmdb --skill-collection --record-failed'.format(args.logdir)],
    ['BC training without skills and depth, action_space = joints',
     'python -m bc.net.train -sd {}/test3 --data {}/test2 -cam 1 -traj 2 -e 2 -bs 4 -da 7 -sa [1,] -ts 3 -a resnet18 --input_type depth --action_space joints'.format(args.logdir, args.logdir)],
    ['BC training with skills and rgbd, action_space = joints',
     'python -m bc.net.train -sd {}/test4 --data {}/test2 -cam 1 -traj 2 -e 2 -bs 4 -da 7 -sa [1,] -ts 3 -a resnet18 --input_type rgbd --skills --action_space joints'.format(args.logdir, args.logdir)]]


def run_test(name, command):
    try:
        print(colored('Running TEST {}'.format(name), 'yellow'))
        subprocess.check_call(command.split(' '))
        print(colored('TEST "{}" OK\n'.format(name), 'green'))
        return 1
    except Exception as e:
        print(colored('TEST "{}" FAILED\n'.format(name), 'green'))
        return 0


def main():
    # remove the test directories first
    for test_dir in glob.glob('{}/test*'.format(args.logdir)):
        shutil.rmtree(test_dir)

    # run the tests
    test_counter = 0
    for name, command in UNITTESTS:
        test_counter += run_test(name, command)
    print(colored('{}/{} TESTS are OK'.format(test_counter, len(UNITTESTS)), 'cyan'))

if __name__ == "__main__":
    main()
