from sacred import Ingredient

model_ingredient = Ingredient('model')
dataset_ingredient = Ingredient('dataset', ingredients=[model_ingredient])
train_ingredient = Ingredient('train', ingredients=[dataset_ingredient])
eval_ingredient = Ingredient('eval', ingredients=[model_ingredient])

@model_ingredient.config
def cfg_model():
    dir = None
    archi = 'resnet18'
    num_frames = 3
    channels = 'depth'
    action_space = 'joints'
    dim_action = 7
    steps_action = [1,10,20,30]
    num_skills = 1

@dataset_ingredient.config
def cfg_dataset():
    dir = None
    max_demos = None
    num_cameras = 1

@train_ingredient.config
def cfg_train():
    lam_grip = 0.1
    batch_size = 64
    learning_rate = 1e-3
    epochs = 200
    workers = 16
    eval_interval=4
    eval_proportion = 0.05
    resume = True

@eval_ingredient.config
def cfg_eval():
    dir = None
    db_type = 'lmdb'
    env = 'UR5-PickCamEnv-v0'
    seed = 0
    episodes = 1
    net_epoch = -1
    max_steps = -1
    timescale = -1
    skill_sequence=[0]
    first_epoch = 2
    last_epoch = 3
    iter_epoch = 2
    workers = 1
    rewrite = True
    record_failed = False
    skill_collection = False
    dask = False
    render = False
