import os
import numpy as np

from tensorboardX import SummaryWriter

train_writer, eval_writer = None, None

def init_writers(logdir):
    global train_writer, eval_writer
    train_writer = SummaryWriter(os.path.join(logdir, 'train'))
    eval_writer = SummaryWriter(os.path.join(logdir, 'eval'))

def add_summary(tag, value, iter, stage='train'):
    if stage == 'train':
        writer = train_writer
    elif stage == 'eval':
        writer = eval_writer
    else:
        raise NotImplementedError
    writer.add_scalar(tag, value, iter)

def train(loss_dict, epoch, batch_id, num_batches):
    for loss_name, loss_list in loss_dict.items():
        loss_value = loss_list[-1]
        add_summary('losses/{}'.format(loss_name), loss_value, epoch * num_batches + batch_id)

def eval(loss_dict, epoch, num_batches):
    for loss_name, loss_list in loss_dict.items():
        loss_value = np.mean(loss_list)
        add_summary('losses/{}'.format(loss_name), loss_value, (epoch + 1) * num_batches, 'eval')

