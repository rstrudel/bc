import torch
import torch.nn as nn

import os
import numpy as np
import json

from ..config import train_ingredient
from ..dataset import ImitationDataset
from ..dataset.utils import Subset
from . import zoo

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

@train_ingredient.capture
def make_loader(model, dataset, batch_size, workers, eval_proportion):

    im_dataset = ImitationDataset(dataset['dir'], dataset['max_demos'], dataset['num_cameras'],
                               model['num_frames'], model['channels'], model['action_space'],
                               model['dim_action'], model['steps_action'], model['num_skills'])
    train_dataset, eval_dataset = split_indices(im_dataset, eval_proportion)
    train_loader = torch.utils.data.DataLoader(
            train_dataset, batch_size, num_workers=workers, shuffle=True)
    if eval_dataset:
        eval_loader = torch.utils.data.DataLoader(
            eval_dataset, batch_size, num_workers=workers, shuffle=True)
    else:
        eval_loader = None

    return train_loader, eval_loader, im_dataset.get_statistics()

@train_ingredient.capture
def make_net(model, dataset, resume, learning_rate, epochs, lam_grip):
    dir_net = model['dir']
    archi = model['archi']

    possible_path = os.path.join(dir_net, '{}_current.pth'.format(archi))
    if resume and os.path.exists(possible_path):
        net_load_path = possible_path
        print('Load network and optimizer from the savedir {}'.format(dir_net))
    else:
        net_load_path = None

    if model['num_skills'] > 1:
        net = zoo.NetworkSkills(
            archi=archi, timesteps=model['num_frames'], input_type=model['channels'],
            action_space=model['action_space'], dim_action=model['dim_action'],
            steps_action=len(model['steps_action']), path=net_load_path,
            num_subpolicies=model['num_skills'], lam_grip=lam_grip)
    else:
        net = zoo.Network(
            archi=archi, timesteps=model['num_frames'], input_type=model['channels'],
            action_space=model['action_space'], dim_action=model['dim_action'],
            steps_action=len(model['steps_action']), path=net_load_path,
            lam_grip=lam_grip)

    net.load_optimizer(learning_rate, epochs)

    print('Net path : {}'.format(dir_net))
    if os.path.isdir(dir_net):
        print('{} already exists.'.format(dir_net))
    else:
        os.mkdir(dir_net)

    return net, net.optimizer, net.scheduler, net.max_epoch, dir_net

@train_ingredient.capture
def write_info(statistics, model, dataset, batch_size, learning_rate, epochs, lam_grip, resume):
    file_info = open(os.path.join(model['dir'], 'info.json'), 'w')
    dic_info = {'archi': model['archi'], 'channels': model['channels'],
                'num_frames': model['num_frames'],
                'action_space': model['action_space'], 'dim_action': model['dim_action'],
                'steps_action': model['steps_action'], 'num_skills': model['num_skills'],
                'statistics': statistics, 'cameras': dataset['num_cameras'],
                'dataset': dataset['dir'], 'max_demos': dataset['max_demos'],
                'lam_grip': lam_grip, 'resume': resume, 'epochs': epochs,
                'learning_rate': learning_rate, 'batch_size': batch_size}
    file_info.write(json.dumps(dic_info, cls=NumpyEncoder))
    file_info.close()

def get_train_loss():
    train_loss = {'l2_move': []}
    train_loss['ce_grip'] = []
    return train_loss

@train_ingredient.capture
def compute_loss(batch, net, model, eval=False):
    obs_images, actions, subpolicy_labels, obs_goals = batch
    loss_planner = None # for non hierarchical setups
    loss_transfer = None # for non goal setup
    if model['num_skills'] > 1:
        loss_grip, loss_move = net.compute_loss(
            {'frames': obs_images}, actions, subpolicy_labels, eval)
    else:
        loss_grip, loss_move = net.compute_loss(
            {'frames': obs_images}, actions, eval)

    return loss_grip, loss_move

def append_losses(train_loss, batch_losses):
    loss_grip, loss_move = batch_losses
    train_loss['l2_move'].append(loss_move.item())
    train_loss['ce_grip'].append(loss_grip.item())

def save_losses(train_loss, dir_net):
    np.savez('{}/loss.npz'.format(dir_net),
            ce_grip=train_loss['ce_grip'],
            l2_move=train_loss['l2_move'])

def run_evaluation(net, loader, epoch, loss_names):
    print('Evaluating after epoch {}'.format(epoch))
    loss_dict = {'ce_grip': [], 'l2_move': []}
    net.net.eval()
    for i, batch in enumerate(loader):
        with torch.no_grad():
            loss_grip, loss_move = compute_loss(batch, net, eval=True)
            loss_dict['ce_grip'].append(loss_grip)
            loss_dict['l2_move'].append(loss_move)
    net.net.train()
    return loss_dict

def split_indices(dataset, eval_proportion):
    if eval_proportion is None:
        train_dataset = dataset
        eval_dataset = None
    else:
        dataset_length = len(dataset)
        eval_length = int(dataset_length * eval_proportion)
        eval_indices = np.arange(dataset_length-eval_length, dataset_length)
        # eval_indices = np.random.choice(dataset_length, eval_length,\
        #                                 replace=False)
        # check if the eval indices are unique
        assert len(set(eval_indices)) == len(eval_indices)
        # train has all the indices minus the eval indices
        train_indices = list(set(range(dataset_length)) - set(eval_indices))
        # check if train and eval do not intersect
        assert len(set(train_indices).intersection(eval_indices)) == 0
        train_dataset = Subset(dataset, train_indices)
        eval_dataset = Subset(dataset, eval_indices)

    return train_dataset, eval_dataset


    # print('\nArchitecture {}'.format(config.archi))
    # print('Input type {}'.format(config.input_type))
    # print('Cameras {}'.format(config.cameras))
    # print('GPUs {}'.format(num_gpu))
    # print('Action space dim {}'.format(config.dim_action))
    # print('Action predicted on steps {}'.format(config.steps_action))
    # print('Size of net output {}'.format(tot_actions))
    # print('Regression {}'.format(config.regression))
    # print('Skills Policies {}'.format(config.skills))
    # print('Goal Conditionned Policy {}'.format(config.goal_conditioned))
