from . import base
import numpy as np

class NetworkReg(base.MetaNetwork):
    "Full Regression Architecture"
    "One head prediciting current action and future actions using concatenated Old Blocks"
    "Gripper state predicted as a continuous signal"

    def __init__(self, archi, timesteps, dim_action, steps_action, 
                 path=None, lam_grip=0.0, input_type='depth'):
        super(NetworkReg, self).__init__()

        self.timesteps = timesteps
        self.dim_action = dim_action
        self.steps_action = steps_action
        self.tot_actions_pred = dim_action*steps_action
        self.tot_actions_gt = dim_action*steps_action
        self.lam_grip = lam_grip
        '''self.action_factors = action_factors.copy()
        # Standard Block to Old Block factors
        self.action_factors[:3] = action_factors[4:7]
        self.action_factors[3] = action_factors[0]
        self.action_factors[4:7] = action_factors[1:4]'''

        # attributes of MetaNetwork
        self.archi = archi
        self.input_dim = self.channels[input_type]*timesteps
        self.output_dim = self.tot_actions_pred

        self.load(archi, path)

    def __call__(self, obs, compute_grad=True):
        assert 'frames' in obs
        obs['frames'] = obs['frames'].to(self.device)
        if not compute_grad:
            torch.set_grad_enabled(False)
        pred = self.net(obs['frames'])
        if not compute_grad:
            torch.set_grad_enabled(True)
        return pred

    def compute_loss(self, obs, actions, eval=False):
        pred = self.__call__(obs)
        actions = actions.to(self.device)

        # Column 0 is for gripper state : apply L2 loss
        # Columns 1 to 6 are the linear and angular velocity regression : apply L2 loss
        loss_move = self.l2_loss(pred, actions)
        loss = loss_move
        if not eval:
            loss.backward()
        return loss_move

    def get_action(self, obs):
        dic_action = OrderedDict()
        
        pred = self.__call__(obs, compute_grad=False)
        pred = pred[0].cpu().numpy()
        pred = pred[:self.dim_action]

        if self.dim_action == 4:
            dic_action['grip_velocity'] = pred[0]
            dic_action['linear_velocity'] = pred[1:4]
        elif self.dim_action == 7:
            dic_action['angular_velocity'] = pred[:3]
            dic_action['grip_velocity'] = pred[3]
            dic_action['linear_velocity'] = pred[4:7]

        return dic_action
