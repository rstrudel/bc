from . import base
import numpy as np
import torch
from collections import deque, OrderedDict

class NetworkSkills(base.VanillaNetwork):
    "Skills Architecture"
    "One head for each sub-policy (skill) which predicts current action and future actions using concatenated Standard Blocks"

    def __init__(self, archi, timesteps, action_space, dim_action, steps_action, num_subpolicies,
                 path=None, lam_grip=0.1, input_type='depth', device=None):
        super(NetworkSkills, self).__init__(archi, timesteps, action_space, dim_action, steps_action, 
                                            path, lam_grip, input_type, device)
        self.timesteps = timesteps
        self.dim_action = dim_action
        self.steps_action = steps_action
        # dim_action+1 for predicted actions as the outputs proba p0,p1 of the gripper begin open,close
        self.tot_actions_pred = (dim_action+1)*steps_action
        self.tot_actions_gt = dim_action*steps_action
        self.lam_grip = lam_grip

        self.num_subpolicies = num_subpolicies
        # attributes of MetaNetwork
        self.archi = archi
        self.input_dim = self.channels[input_type]*timesteps
        # a head of size dim_action+1 for each sub-policy
        self.output_dim = self.tot_actions_pred*self.num_subpolicies
        self.current_subpolicy = 0

        self.net = self.architectures[archi](pretrained=False, input_dim=self.input_dim, dim_action=self.tot_actions_pred, num_skills=num_subpolicies,
        num_classes=self.tot_actions_pred*num_subpolicies)

        self.load(archi, path)

    # pred contains the control output by every subpolicies
    # target contains only the gt control gt for the gt subpolicy
    def compute_loss(self, obs, target_ctrl, target_subpolicy, eval=False):
        pred_subpolicies = self.__call__(obs)
        target_ctrl = target_ctrl.to(self.device)
        target_subpolicy = target_subpolicy.to(self.device)

        size_head_subpolicy = self.tot_actions_pred
        size_gt_subpolicy = self.tot_actions_gt

        # extract pred on subpolicies picked according to ground truth
        # this is the only subpolicy where ground truth is available
        mask = torch.zeros(pred_subpolicies.shape, dtype=torch.uint8)
        for i, gt_subpolicy in enumerate(target_subpolicy):
            mask[i, gt_subpolicy*size_head_subpolicy:(gt_subpolicy+1)*size_head_subpolicy] = 1
        pred_subpolicies_gt = pred_subpolicies[mask].view(-1, size_head_subpolicy)

        # index of the beginning of each basic blocs when multiple actions are predicted
        idx_net = np.arange(0, self.tot_actions_pred, self.dim_action+1)
        idx_action = np.arange(0, self.tot_actions_gt, self.dim_action)
        loss_grip = 0
        loss_move = 0
        # each basic bloc is separated in two parts
        # first two columns is the gripper state classification : apply CE loss
        # columns 2 to 7 are the linear and angular velocity regression : apply L2 loss
        for idx_n, idx_a in zip(idx_net, idx_action):
            loss_grip += self.ce_loss(pred_subpolicies_gt[:, idx_n:idx_n+2],
                                      (target_ctrl[:, idx_a] < 0).long())
            loss_move += self.l2_loss(pred_subpolicies_gt[:, idx_n+2:idx_n+self.dim_action+1],
                                      target_ctrl[:, idx_a+1:idx_a+self.dim_action])
        loss = self.lam_grip*loss_grip+(1-self.lam_grip)*loss_move
        if not eval:
            loss.backward()
        return loss_grip, loss_move

    def get_action(self, obs, skill):
        pred = self.__call__(obs, compute_grad=False)
        size_head_subpolicy = self.tot_actions_pred
        pred = pred[0].cpu().numpy()

        # control predicted by the planner chosen subpolicy
        pred_subpolicy = pred[skill*size_head_subpolicy:(skill+1)*size_head_subpolicy]
        pred = pred_subpolicy[:self.dim_action+1]
        return pred
