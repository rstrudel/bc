from . import base
import numpy as np
import torch
from collections import deque

class NetworkHierarchical(base.VanillaNetwork):
    "Hierarchical Architecture"
    "One head prediciting subpolicy to perform and one head for each subpolicy which prediict current action and future actions using concatenated Standard Blocks"

    def __init__(self, archi, timesteps, dim_action, steps_action, num_subpolicies,
                 path=None, lam_grip=0.1, lam_planner=0.2, input_type='depth', device=None):
        super(NetworkHierarchical, self).__init__(archi, timesteps, dim_action, steps_action, 
                                                  path, lam_grip, input_type, device)
        self.timesteps = timesteps
        self.dim_action = dim_action
        self.steps_action = steps_action
        self.tot_actions_pred = (dim_action+1)*steps_action
        self.tot_actions_gt = dim_action*steps_action
        self.lam_grip = lam_grip
        self.lam_planner = lam_planner

        assert num_subpolicies >= 2
        self.num_subpolicies = num_subpolicies
        self.last_pred_planner = deque(maxlen=1)
        
        # attributes of MetaNetwork
        self.archi = archi
        self.input_dim = self.channels[input_type]*timesteps
        # one head for classifying of the size num_subpolicies
        # a head of size dim_action+1 for each subtask
        self.output_dim = (1+self.tot_actions_pred)*self.num_subpolicies
        self.current_subpolicy = 0

        self.load(archi, path)
        

    # pred contains the planner prediction on which subpolicy to choose and the control output by every subpolicies
    # target contains the planner gt and only the control gt for the subpolicy gt 
    def compute_loss(self, obs, target_ctrl, target_planner, target_subpolicy, eval=False):
        pred = self.__call__(obs)
        target_ctrl = target_ctrl.to(self.device)
        target_planner = target_planner.to(self.device)
        target_subpolicy = target_subpolicy.to(self.device)

        size_head_subpolicy = self.tot_actions_pred
        size_gt_subpolicy = self.tot_actions_gt
        pred_planner = pred[:, :self.num_subpolicies]
        loss_planner = self.ce_loss(pred_planner, target_planner)

        # extract pred on subpolicies picked according to ground truth
        # this is the only subpolicy where ground truth is available
        pred_subpolicies = pred[:, self.num_subpolicies:]
        mask = torch.zeros(pred_subpolicies.shape, dtype=torch.uint8)
        for i, gt_subpolicy in enumerate(target_subpolicy):
            mask[i, gt_subpolicy*size_head_subpolicy:(gt_subpolicy+1)*size_head_subpolicy] = 1
        pred_subpolicies_gt = pred_subpolicies[mask].view(-1, size_head_subpolicy)
        '''pred_subpolicies_gt = torch.zeros((target_ctrl.shape[0], size_head_subpolicy))
        pred_subpolicies_gt = pred_subpolicies_gt.to(self.device)
        for i, pred_row in enumerate(pred_subpolicies):
            gt_subpolicy = target_subpolicy[i]
            pred_subpolicies_gt[i] = pred_row[gt_subpolicy*size_head_subpolicy:(gt_subpolicy+1)*size_head_subpolicy]'''

        # index of the beginning of each basic blocs when multiple actions are predicted
        idx_net = np.arange(0, self.tot_actions_pred, self.dim_action+1)
        idx_action = np.arange(0, self.tot_actions_gt, self.dim_action)
        loss_grip = 0
        loss_move = 0
        # each basic bloc is separated in two parts
        # first two columns is the gripper state classification : apply CE loss
        # columns 2 to 7 are the linear and angular velocity regression : apply L2 loss
        for idx_n, idx_a in zip(idx_net, idx_action):
            loss_grip += self.ce_loss(pred_subpolicies_gt[:, idx_n:idx_n+2], 
                                      (target_ctrl[:, idx_a] < 0).long())
            loss_move += self.l2_loss(pred_subpolicies_gt[:, idx_n+2:idx_n+self.dim_action+1], 
                                      target_ctrl[:, idx_a+1:idx_a+self.dim_action])
        loss = self.lam_planner*loss_planner+(1-self.lam_planner)*(self.lam_grip*loss_grip+(1-self.lam_grip)*loss_move)
        if not eval:
            loss.backward()
        return loss_planner, loss_grip, loss_move

    def get_action(self, obs):
        pred = self.__call__(obs, compute_grad=False)
        size_head_subpolicy = self.tot_actions_pred
        pred = pred[0].cpu().numpy()
        # index of the subpolicy to choose predicted by the planner
        pred_planner = pred[:self.num_subpolicies].argmax()
        self.current_subpolicy = pred_planner
        # self.last_pred_planner.append(pred_planner)
        # pred_planner = np.argmax(np.bincount(self.last_pred_planner))
        # controls predicted by subpolicies
        pred_subpolicies = pred[self.num_subpolicies:]
        # control predicted by the planner chosen subpolicy
        pred_subpolicy = pred_subpolicies[pred_planner*size_head_subpolicy:(pred_planner+1)*size_head_subpolicy]
        pred = pred_subpolicy[:self.dim_action+1]
        return pred
