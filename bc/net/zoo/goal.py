from . import base
import torch
import numpy as np
from collections import OrderedDict


class NetworkGoal(base.VanillaNetwork):
    "Goal Architecture"
    "One head prediciting current action and future actions using concatenated Standard Blocks"
    "The policy is goal conditionned, a goal observation is fed to the network"
    "The goal feature is computed and at each forward pass the current observation and goal"
    "observation are concatenated afer the average pooling layer"
    "From feature space to ouput:" 
    "x : (512,) goal : (512,) ; cat(x, goal) -> 1024 x 512 -> 512 x dim_output"

    def __init__(self, archi, timesteps, dim_action, steps_action,
                 path=None, lam_grip=0.1, input_type='depth', device=None):
        super(NetworkGoal, self).__init__(archi, timesteps, dim_action, steps_action, 
                                          path, lam_grip, input_type, device)
        # additional input for goal image
        # self.input_dim += self.channels[input_type]
        self.goal = None

        self.load(archi, path)

    '''def set_goal(self, obs_goal):
        self._goal_frames = obs_goal.clone()
        obs_goal = obs_goal.to(self.device)
        self.net.module.set_goal(obs_goal)
        # hook = self.net.module.avgpool.register_forward_hook(self._store_goal)
        # self.__call__(obs, compute_grad=False)
        # hook.remove()
    
    def set_option(self, option, obs=None):
        option = option.to(self.device)
        self.net.module.set_option(option)

    def set_goal_from_option(self, obs, option):
        obs = obs.to(self.device)
        option = option.to(self.device)
        self.net.module.set_goal_from_option(obs, option)

    def mse_loss(self, input, target):
        return torch.sum((input - target)**2) / input.data.nelement()'''

    def set_goal(self, goal):
        self.goal = goal.to(self.device)
        self.net.module.set_goal(self.goal)

    def compute_loss(self, obs, actions, goal, eval=False):
        self.set_goal(goal)
        # self.set_option(obs['option'])
        pred = self.__call__(obs)
        actions = actions.to(self.device)

        # index of the beginning of each basic blocs when multiple actions are predicted
        idx_net = np.arange(0, self.tot_actions_pred, self.dim_action+1)
        idx_action = np.arange(0, self.tot_actions_gt, self.dim_action)
        loss_grip = 0
        loss_move = 0
        # Each basic bloc is separated in two parts
        # First two columns is the gripper state classification : apply CE loss
        # Columns 2 to 7 are the linear and angular velocity regression : apply L2 loss
        for idx_n, idx_a in zip(idx_net, idx_action):
            loss_grip += self.ce_loss(pred[:, idx_n:idx_n+2], 
                                      (actions[:, idx_a] < 0).long())
            loss_move += self.l2_loss(pred[:, idx_n+2:idx_n+self.dim_action+1], 
                                      actions[:, idx_a+1:idx_a+self.dim_action])
        # loss = 0.5*(self.lam_grip*loss_grip+(1-self.lam_grip)*loss_move)+0.5*loss_transfer
        loss = self.lam_grip*loss_grip+(1-self.lam_grip)*loss_move
        if not eval:
            loss.backward()
        return loss_grip, loss_move

    def get_action(self, obs):
        pred, = self.__call__(obs, compute_grad=False)
        pred = pred.cpu().numpy()
        pred = pred[:self.dim_action+1]
        return pred

    def get_dic_action(self, obs, skill=None):
        if self.goal is None:
            raise ValueError('Goal frame is not defined.')

        dic_action = OrderedDict()
        # obs = obs['frames'][0].to(self.device)
        # obs_goal = torch.cat((obs, self.goal), 0)
        # print(obs_goal.size())
        pred = self.get_action(obs)
    
        dic_action['grip_velocity'] = -1+2*(pred[0]>pred[1])
        dic_action['linear_velocity'] = pred[2:5]
        if self.dim_action == 7:
            dic_action['angular_velocity'] = pred[5:8]

        return dic_action
