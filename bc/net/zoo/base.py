import torch
import torch.nn as nn
import torch.optim as optim

import numpy as np
import glob
import os
from collections import OrderedDict, deque

from ..architectures import conv_net, attention_net, alexnet, vgg, resnet, densenet

# Block stacked if predicting multiple actions

# Standard Block:
# 2x3x3 : 2 Binary Clf Grip - 3 Linear Vel - 3 Angular Vel
# Grip Open 0 Grip Closed 1

# Old Block :
# 1x3 : 1 Grip Vel 3 Linear Vel 
# 3x1x3 : 3 Angular Vel 1 Grip Vel 3 Linear Vel
# Grip seen as continous signal

class MetaNetwork:
    "Base class for network"

    def __init__(self):
        self.architectures={
            'cnn': conv_net.DepthNet,
            'att': attention_net.CNN,
            'alexnet': alexnet.AlexNet,
            'vgg11_bn': vgg.vgg11_bn,
            'vgg16_bn': vgg.vgg16_bn,
            'sresnet10': resnet.small_resnet10,
            'sresnet10fc': resnet.small_resnet10_fc,
            'resnet10': resnet.resnet10,
            'resnet18': resnet.resnet18,
            'standard_resnet18': resnet.standard_resnet18,
            'resnet18fc': resnet.resnet18_fc,
            'resnet50': resnet.resnet50,
            'resnet101': resnet.resnet101,
            'resnet152': resnet.resnet152,

            'resnet18_fc64': resnet.resnet18_fc64,
            'resnet18_featbranch': resnet.resnet18_featbranch,
            'resnet18_b4branch': resnet.resnet18_b4branch,

            'skills_resnet18': resnet.skills_resnet18,
            'skills_sresnet10': resnet.skills_small_resnet10,
            'goal_resnet18': resnet.goal_resnet18,
            'goal_sresnet10': resnet.goal_small_resnet10,
            'dann_resnet18': resnet.dann_resnet18,
        }

        self.channels = {'depth': 1, 'rgb': 3, 'rgbd': 4}

        self.net = None
        self.optimizer = None
        self.archi = None
        self.input_dim = None
        self.output_dim = None
        self.action_space = None
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.device_forced = False  # flag to figure out if the device was set manually in a child class
        self.model_dict = None
        self.max_epoch = 0
        self.l2_loss = nn.MSELoss()
        self.ce_loss = nn.CrossEntropyLoss()

    def load(self, archi, path):
        assert archi in self.architectures
        assert type(self.input_dim) is int and type(self.output_dim) is int

        if path is not None:
            savedir_path = path[:path.rfind('/')]
            self.find_max_epoch(savedir_path)
            checkpoint_path = path
        else:
            checkpoint_path = None

        # load model_dict if a path is given
        if checkpoint_path:
            # load the network weights
            if str(self.device) == 'cpu':
                self.model_dict = torch.load(
                    checkpoint_path, map_location=lambda storage, loc: storage)
            else:
                self.model_dict = torch.load(checkpoint_path)

        # define network and pass it in parallel mode if possible
        if self.net is None:
            self.net = self.architectures[archi](
                pretrained=False, input_dim=self.input_dim, num_classes=self.output_dim)

        # if device_forced, force the network to stay on cpu even when cuda is available
        if self.device_forced and str(self.device) == 'cpu':
            pass
        else:
            self.net = nn.DataParallel(self.net)

        # load network checkpoint if available
        if self.model_dict is not None:
            # retro-compatibility with the old way of saving checkpoints
            if 'net_state_dict' in self.model_dict:
                net_state_dict = self.model_dict['net_state_dict']
            else:
                net_state_dict = self.model_dict
            if self.device_forced:
                net_state_dict = self._rename_state_dict(net_state_dict, self.device)
            self.net.load_state_dict(net_state_dict)

        # loading model on GPU if available
        self.net.to(self.device)

    def load_optimizer(self, learning_rate, epochs):
        # define optimizer
        self.optimizer = optim.Adam([{'params': self.net.parameters(), 'initial_lr': learning_rate}])
        milestones = 80+np.arange(0, epochs, 80)
        self.scheduler = optim.lr_scheduler.MultiStepLR(self.optimizer, milestones=milestones, gamma=0.1)
        # load optimizer checkpoint if available
        if self.model_dict is not None:
            target_device = 'cpu' if self.device.type == 'cpu' else 'cuda'
            if 'optimizer_state_dict' in self.model_dict:
                # load the optimizer weights
                self.optimizer.load_state_dict(self.model_dict['optimizer_state_dict'])
                for state in self.optimizer.state.values():
                    for k, v in state.items():
                        if isinstance(v, torch.Tensor):
                            state[k] = getattr(v, target_device)()
            if 'scheduler_state_dict' in self.model_dict:
                self.scheduler.load_state_dict(self.model_dict['scheduler_state_dict'])

    def find_max_epoch(self, path):
        checkpoints = glob.glob(os.path.join(path, '{}_*.pth'.format(self.archi)))
        for checkpoint_path in checkpoints:
            epoch_str = checkpoint_path.split('/{}_'.format(self.archi))[-1].replace('.pth', '')
            try:
                self.max_epoch = max(int(epoch_str), self.max_epoch)
            except:
                pass

    def __call__(self, obs, compute_grad):
        raise NotImplementedError

    def set_eval(self):
        self.net.eval()

    def get_n_param(self):
        n_param = 0
        for parameter in self.net.parameters():
            shape = parameter.size()
            size = 1
            for dim in shape:
                size *= dim
            n_param += size
        return n_param

    # compute loss and gradients for a batch of observation, actions
    # obs : N x input_dim x 224 x 224
    def compute_loss(self, **kwargs):
        raise NotImplementedError

    def get_action(self, **kwargs):
        raise NotImplementedError

    def get_dic_action(self, **kwargs):
        raise NotImplementedError

    def save(self, path):
        model_dict = {'net_state_dict': self.net.state_dict(),
                    'optimizer_state_dict': self.optimizer.state_dict(),
                    'scheduler_state_dict': self.scheduler.state_dict()}
        torch.save(model_dict, path)

    def _rename_state_dict(self, state_dict, device):
        state_dict_renamed = {}
        for key, value in state_dict.items():
            if str(device) == 'cpu':
                new_key = key.replace('module.', '')
            else:
                if 'module.' in key:
                    # state_dict was saved on GPU, no need to rename
                    new_key = key
                else:
                    # state_dict was saved on CPU, append 'module.'
                    new_key = 'module.' + key
            state_dict_renamed[new_key] = value
        return state_dict_renamed


class VanillaNetwork(MetaNetwork):
    "Vanilla Architecture"
    "One head prediciting current action and future actions using concatenated Standard Blocks"

    def __init__(self, archi, timesteps, action_space, dim_action, steps_action, 
                 path=None, lam_grip=0.1, input_type='depth', device=None):
        super(VanillaNetwork, self).__init__()

        self.timesteps = timesteps
        self.action_space = action_space
        self.dim_action = dim_action
        self.steps_action = steps_action
        self.tot_actions_pred = (dim_action+1)*steps_action
        self.tot_actions_gt = dim_action*steps_action
        self.lam_grip = lam_grip

        # attributes of MetaNetwork
        self.archi = archi
        self.input_dim = self.channels[input_type]*timesteps
        self.output_dim = self.tot_actions_pred
        if device:
            self.device = torch.device(device)
        self.device_forced = device is not None

    def __call__(self, obs, compute_grad=True):
        assert 'frames' in obs
        obs['frames'] = obs['frames'].to(self.device)

        if not compute_grad:
            torch.set_grad_enabled(False)
        pred = self.net(obs['frames'])
        if not compute_grad:
            torch.set_grad_enabled(True)
        return pred

    def get_dic_action(self, obs, skill=None):
        dic_action = OrderedDict()
        if skill is not None:
            pred = self.get_action(obs, skill)
        else:
            pred = self.get_action(obs)

        action_space = self.action_space
        dic_action['grip_velocity'] = -1+2*(pred[0]>pred[1])
        if action_space == 'tool':
            dic_action['linear_velocity'] = pred[2:5]
            if self.dim_action == 7:
                dic_action['angular_velocity'] = pred[5:8]
        elif action_space == 'joints':
            dic_action['joint_velocity'] = pred[2:8]
        else:
            raise ValueError('Unknown action space : {}'.format(action_space))

        return dic_action

    # hook on average pooling to check gradients
    def set_backward_hook(self):
        grad_in, grad_out = deque(maxlen=10), deque(maxlen=10)
        def print_gradnorm(self, grad_input, grad_output):
            print('Inside ' + self.__class__.__name__ + ' backward')
            print('Inside class:' + self.__class__.__name__)
            print('')
            print('grad_input size:', grad_input[0].size())
            print('grad_output size:', grad_output[0].size())
            grad_in.append(grad_input[0].norm())
            grad_out.append(grad_output[0].norm())
            print('grad_input norm:', sum(grad_in)/len(grad_in))
            print('grad_output norm:', sum(grad_out)/len(grad_out))
        self.net.module.avgpool.register_backward_hook(print_gradnorm)


class Network(VanillaNetwork):
    "Vanilla Architecture"
    "One head prediciting current action and future actions using concatenated Standard Blocks"

    def __init__(self, archi, timesteps, action_space, dim_action, steps_action,
                 path=None, lam_grip=0.1, input_type='depth', device=None):
        super(Network, self).__init__(archi, timesteps, action_space, dim_action, steps_action,
                 path, lam_grip, input_type)
        self.load(archi, path)

    def compute_loss(self, obs, actions, eval=False):
        pred = self.__call__(obs)
        actions = actions.to(self.device)

        # index of the beginning of each basic blocs when multiple actions are predicted
        idx_net = np.arange(0, self.tot_actions_pred, self.dim_action+1)
        idx_action = np.arange(0, self.tot_actions_gt, self.dim_action)
        loss_grip = 0
        loss_move = 0
        # Each basic bloc is separated in two parts
        # First two columns is the gripper state classification : apply CE loss
        # Columns 2 to 7 are the linear and angular velocity regression : apply L2 loss
        for idx_n, idx_a in zip(idx_net, idx_action):
            loss_grip += self.ce_loss(pred[:, idx_n:idx_n+2],
                                      (actions[:, idx_a] < 0).long())
            loss_move += self.l2_loss(pred[:, idx_n+2:idx_n+self.dim_action+1],
                                      actions[:, idx_a+1:idx_a+self.dim_action])
        loss = self.lam_grip*loss_grip+(1-self.lam_grip)*loss_move
        if not eval:
            loss.backward()
        return loss_grip, loss_move

    def get_action(self, obs, skill=None):
        pred = self.__call__(obs, compute_grad=False)
        pred = pred[0].cpu().numpy()
        pred = pred[:self.dim_action+1]
        return pred
