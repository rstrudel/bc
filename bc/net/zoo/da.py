from . import base
import numpy as np

class NetworkDA(base.VanillaNetwork):
    "Domain Adversarial Network"
    "One head prediction actions using concatenated Standard Blocks"
    "One head predicting the domain of the image : Source vs Target"

    def __init__(self, archi, timesteps, dim_action, steps_action, 
                 path=None, lam_grip=0.1, input_type='depth', device=None):
        super(NetworkDA, self).__init__(archi, timesteps, dim_action, steps_action, 
                                        path, lam_grip, input_type, device)

        self.load(archi, path)        

    def __call__(self, obs, alpha, compute_grad=True):
        assert 'frames' in obs
        obs['frames'] = obs['frames'].to(self.device)
        if not compute_grad:
            torch.set_grad_enabled(False)
        pred_ctrl, pred_dmn = self.net(obs['frames'], alpha)
        if not compute_grad:
            torch.set_grad_enabled(True)
        return pred_ctrl, pred_dmn

    def compute_loss(self, obs, alpha, actions, idxs_dataset, idxs_supervised, eval=False):
        pred_ctrl, pred_dmn = self.__call__(obs, alpha)
        actions = actions.to(self.device)
        idxs_dataset = idxs_dataset.to(self.device)
        idxs_supervised = idxs_supervised.to(self.device)
        
        # semi-supervised transfer learning: only do supervised control on target on given indexes
        # every row of prediction and ground truth with idx_supervised == 0 is set to 0
        # mask = (idxs_supervised == 1).float().view(-1, 1)
        # pred_ctrl = pred_ctrl*mask
        # actions = actions*mask
        pred_ctrl = pred_ctrl[idxs_supervised==1]
        actions = actions[idxs_supervised==1]
        # print('shape', pred_ctrl.shape, actions.shape)

        # loss on control
        idx_net = np.arange(0, self.tot_actions_pred, self.dim_action+1)
        idx_action = np.arange(0, self.tot_actions_gt, self.dim_action)
        loss_grip = 0
        loss_move = 0
        for idx_n, idx_a in zip(idx_net, idx_action):
            loss_grip += self.ce_loss(pred_ctrl[:, idx_n:idx_n+2], 
                                    (actions[:, idx_a] < 0).long())
            loss_move += self.l2_loss(pred_ctrl[:, idx_n+2:idx_n+self.dim_action+1], 
                                    actions[:, idx_a+1:idx_a+self.dim_action])
        loss_ctrl = self.lam_grip*loss_grip+(1-self.lam_grip)*loss_move

        # loss on domain adaptation
        loss_dmn = self.ce_loss(pred_dmn, idxs_dataset)

        # sum of losses
        loss = loss_ctrl+loss_dmn
        if not eval:
            loss.backward()

        return loss_grip, loss_move, loss_dmn

    def get_action(self, obs):
        pred_ctrl, pred_dmn = self.__call__(obs, 0, compute_grad=False)
        pred_ctrl = pred_ctrl[0].cpu().numpy()
        pred_ctrl = pred_ctrl[:self.dim_action+1]
        return pred_ctrl
