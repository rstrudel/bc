from torch.autograd import Function
import torch.nn as nn


class DomainBN(nn.Module):
    def __init__(self, nb_channels, dim):
        super(DomainBN, self).__init__()
        bn_dims = [nn.BatchNorm1d, nn.BatchNorm2d]
        self.bn_source = bn_dims[dim-1](nb_channels)
        self.bn_target = bn_dims[dim-1](nb_channels)

    # batch tensor (N, C, H, W)
    # label domain tensor (N, 1)
    def __call__(self, x, label):
        source_idxs = label == 0

        if source_idxs.sum() > 0:
            x[source_idxs] = self.bn_source(x[source_idxs])
        if (1-source_idxs).sum() > 0:
            x[1 - source_idxs] = self.bn_target(x[1 - source_idxs])

        return x


class FReverseLayer(Function):
    @staticmethod
    def forward(ctx, x, alpha):
        # context variable used to stash information for backward computation
        ctx.alpha = alpha
        return x

    @staticmethod
    def backward(ctx, grad_output):
        output = -grad_output*ctx.alpha
        return output, None