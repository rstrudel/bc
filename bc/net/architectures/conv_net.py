import torch
import torch.nn as nn
import torch.nn.functional as F

class CNN_Feat(nn.Module):
    def __init__(self, time_steps):
        super(CNN_Feat, self).__init__()

        # kernel and maxpool
        self.conv1 = nn.Conv2d(time_steps, 64, kernel_size=5, stride=2)
        self.pool1 = nn.MaxPool2d(2, 2)
        self.conv2a = nn.Conv2d(64, 128, kernel_size=3)
        self.conv2b = nn.Conv2d(128, 128, kernel_size=3)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.conv3a = nn.Conv2d(128, 256, kernel_size=3)
        self.conv3b = nn.Conv2d(256, 256, kernel_size=3)
        self.pool3 = nn.MaxPool2d(2, 2)

        # norm layer
        self.bn1 = nn.BatchNorm2d(64)
        self.bn2a = nn.BatchNorm2d(128)
        self.bn2b = nn.BatchNorm2d(128)
        self.bn3a = nn.BatchNorm2d(256)
        self.bn3b = nn.BatchNorm2d(256)

        # fc layers
        self.fc1 = nn.Linear(256*10*10, 1024)
        
        # dropout
        self.drop1 = nn.Dropout(p=0.5)

    def forward(self, x, y):
        x = F.relu(self.bn1(self.conv1(x)))
        x = self.pool1(x)
        x = F.relu(self.bn2a(self.conv2a(x)))
        x = F.relu(self.bn2b(self.conv2b(x)))
        x = self.pool2(x)
        x = F.relu(self.bn3a(self.conv3a(x)))
        x = F.relu(self.bn3b(self.conv3b(x)))
        x = self.pool3(x)
        
        x = x.view(-1, 256*10*10)
        x = self.drop1(F.relu(self.fc1(x)))

        return x


class DepthNet(nn.Module):
    def __init__(self, time_steps, num_classes):
        super(DepthNet, self).__init__()

        self.cnn_feat = CNN_Feat(time_steps)
        self.fc1 = nn.Linear(1024, num_classes)
        # self.fc2 = nn.Linear(1024, num_classes)
        # self.drop1 = nn.Dropout(p=0.5)
        
    def forward(self, x, y):
        z = self.cnn_feat(x[:, 0], y)
        # z = self.drop1(F.relu(self.fc1(z)))
        # z = torch.cat((x, y), 1)
        z = self.fc1(z)

        return z


class DepthNet_MultiCam(nn.Module):
    def __init__(self, time_steps, output_shape):
        super(DepthNet_MultiCam, self).__init__()

        self.cnn_feat = CNN_Feat(time_steps)
        self.ln1 = nn.LayerNorm(1024)
        self.ln2 = nn.LayerNorm(1024)

        self.fc1 = nn.Linear(2048, 2048)
        self.fc2 = nn.Linear(2048, output_shape)

        self.drop1 = nn.Dropout(p=0.5)
        self.drop2 = nn.Dropout(p=0.5)

    def forward(self, x, y):
        z1 = self.cnn_feat(x[:, 0], y)
        z2 = self.cnn_feat(x[:, 1], y)

        t = torch.cat((z1,z2), 1)
        t = self.drop1(F.relu(self.ln1(self.fc1(t))))
        t = self.fc2(t)

        return t


class CNN_std(nn.Module):
    def __init__(self, time_steps, output_shape, input_shape=(320, 240, 3)):
        super(CNN_std, self).__init__()

        # kernel and maxpool
        self.conv1 = nn.Conv2d(time_steps, 64, kernel_size=5, stride=2)
        self.pool1 = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(64, 128, kernel_size=5)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.conv3 = nn.Conv2d(128, 256, kernel_size=3)
        self.pool3 = nn.MaxPool2d(2, 2)

        # fc layers
        self.fc1 = nn.Linear(256 * 9 * 12, 1024)
        self.fc2 = nn.Linear(1024, 1024)
        self.fc3 = nn.Linear(1024, output_shape)

        # dropout
        self.drop1 = nn.Dropout(p=0.5)
        self.drop2 = nn.Dropout(p=0.5)

    def forward(self, x, y):
        x = x[:, 0]
        x = self.pool1(F.relu(self.conv1(x)))
        x = self.pool2(F.relu(self.conv2(x)))
        x = self.pool3(F.relu(self.conv3(x)))

        x = x.view(-1, 256 * 9 * 12)
        x = self.drop1(F.relu(self.fc1(x)))

        # z = torch.cat((x, y), 1)
        z = x
        z = self.drop2(F.relu(self.fc2(z)))
        z = self.fc3(z)

        return z


class CNN_BatchNorm(nn.Module):
    def __init__(self, time_steps, output_shape, input_shape=(320, 240, 3)):
        super(CNN_BatchNorm, self).__init__()

        # kernel and maxpool
        self.conv1 = nn.Conv2d(time_steps, 64, kernel_size=5, stride=2)
        self.pool1 = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(64, 64, kernel_size=3)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3)
        self.pool3 = nn.MaxPool2d(2, 2)

        # fc layers
        self.fc1 = nn.Linear(64*9*13, 64)
        self.fc2 = nn.Linear(64, 32)
        self.fc3 = nn.Linear(32, output_shape)

        # dropout
        self.drop1 = nn.Dropout(p=0.2)
        #self.drop2 = nn.Dropout(p=0.2)

        # batch norm
        self.bn1 = nn.BatchNorm2d(64)
        self.bn2 = nn.BatchNorm2d(64)
        self.bn3 = nn.BatchNorm2d(64)
        self.bn4 = nn.BatchNorm1d(64)
        #self.bn5 = nn.BatchNorm1d(32)

    def _initialize_weights(self):
        for m in self.modules():
            m.weight.data.uniform_(-0.01, 0.01)

    def forward(self, x, y):
        x = self.pool1(F.relu(self.bn1(self.conv1(x))))
        x = self.pool2(F.relu(self.bn2(self.conv2(x))))
        x = self.pool3(F.relu(self.bn3(self.conv3(x))))

        x = x.view(-1, 64*9*13)
        x = self.drop1(F.relu(self.bn4(self.fc1(x))))
        #x = F.relu(self.bn4(self.fc1(x)))

        #z = torch.cat((x, y), 1)
        z = x
        z = F.relu(self.fc2(z))
        z = self.fc3(z)

        return z
