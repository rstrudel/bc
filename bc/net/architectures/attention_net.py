import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class CNN(nn.Module):
    def __init__(self, time_steps, output_shape, input_shape=(320, 240, 3)):
        super(CNN, self).__init__()

        # kernel
        self.conv1 = nn.Conv2d(time_steps, 64, kernel_size=5, stride=2)
        self.conv2 = nn.Conv2d(64, 64, kernel_size=5, stride=2)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3)
        self.conv4 = nn.Conv2d(64, 128, kernel_size=3)
        self.conv5 = nn.Conv2d(128, 128, kernel_size=3)
        
        # norm layer
        self.ln1 = nn.LayerNorm((64, 88, 118))
        self.ln2 = nn.LayerNorm((64, 42, 57))
        self.ln3 = nn.LayerNorm((64, 40, 55))
        self.ln4 = nn.LayerNorm((128, 38, 53))
        self.ln5 = nn.LayerNorm((128, 36, 51))
        self.ln6 = nn.LayerNorm(512)
        self.ln7 = nn.LayerNorm(1024)

        # fc layers
        self.fc1 = nn.Linear(256, 512)
        self.fc2 = nn.Linear(512, 1024)
        self.fc3 = nn.Linear(1024, output_shape)

        # dropout
        self.drop1 = nn.Dropout(p=0.5)
        self.drop2 = nn.Dropout(p=0.5)

        # row, col : shape H, W
        num_row = 36
        num_col = 51
        row = (np.arange(num_row)-num_row/2)/num_row
        col = (np.arange(num_col)-num_col/2)/num_col
        row = np.tile(row[:, None], (1, num_col))
        col = np.tile(col[None, :], (num_row, 1))
        self._row = nn.Parameter(torch.Tensor(row))
        self._col = nn.Parameter(torch.Tensor(col))


    def forward(self, x, y):
        x = x[:, 0]
        x = F.relu(self.ln1(self.conv1(x)))
        x = F.relu(self.ln2(self.conv2(x)))
        x = F.relu(self.ln3(self.conv3(x)))
        x = F.relu(self.ln4(self.conv4(x)))
        x = F.relu(self.ln5(self.conv5(x)))
        
        # attention layer
        # t: shape B, C, H, W
        t = torch.exp(-x)
        # p: shape B, C, H, W
        p = t/t.sum(dim=2).sum(dim=2)[:, :, None, None]
        # e: shape B, C, 2
        e_x = (self._row[None, None, :]*p).sum(dim=2).sum(dim=2)
        e_y = (self._col[None, None, :]*p).sum(dim=2).sum(dim=2)
        e = torch.cat((e_x, e_y), 1)

        e = e.view(-1, 128*2)
        x = self.drop1(F.relu(self.ln6(self.fc1(e))))
        #x = F.relu(self.bn5(self.fc1(e)))
        
        #z = torch.cat((x, y), 1)
        z = x
        z = self.drop2(F.relu(self.ln7(self.fc2(z))))
        z = F.tanh(self.fc3(z))

        return z
