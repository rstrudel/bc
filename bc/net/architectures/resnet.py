import torch
import torch.nn as nn
import math
import torch.utils.model_zoo as model_zoo
import torch.nn.functional as F
from .utils import FReverseLayer

__all__ = ['ResNet', 'resnet18', 'resnet34', 'resnet50', 'resnet101',
           'resnet152']

model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
}


def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * self.expansion, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class ResNet(nn.Module):

    def __init__(
            self, block, layers, block_depth=[64, 128, 256, 512],
            input_dim=3, num_classes=1000, return_features=False, **kwargs):
        self.inplanes = 64
        super(ResNet, self).__init__()
        self.conv1 = nn.Conv2d(input_dim, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, block_depth[0], layers[0])
        self.layer2 = self._make_layer(block, block_depth[1], layers[1], stride=2)
        self.layer3 = self._make_layer(block, block_depth[2], layers[2], stride=2)
        self.layer4 = self._make_layer(block, block_depth[3], layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)

        # fully connected layers on top of features
        self.fc = nn.Linear(block_depth[3]*block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
        # whether to give back the features in the forward pass (e.g. for RL master)
        self.return_features = return_features

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        features = x.view(x.size(0), -1)
        x = self.fc(features)

        return x if not self.return_features else (x, features)


class ResNetFC(nn.Module):

    def __init__(self, block, layers, block_depth=[64, 128, 256, 512], fc_hidden=[], 
                input_dim=3, num_classes=1000, return_features=False, **kwargs):
        self.inplanes = 64
        super(ResNetFC, self).__init__()
        self.conv1 = nn.Conv2d(input_dim, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, block_depth[0], layers[0])
        self.layer2 = self._make_layer(block, block_depth[1], layers[1], stride=2)
        self.layer3 = self._make_layer(block, block_depth[2], layers[2], stride=2)
        self.layer4 = self._make_layer(block, block_depth[3], layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)

        # fully connected layers on top of features
        fc_size = [block_depth[3]*block.expansion]+fc_hidden+[num_classes]
        fcs = []
        for i in range(len(fc_size)-1):
            fcs.append(nn.Linear(fc_size[i], fc_size[i+1]))
            if i < len(fc_size)-2:
                # fcs.append(nn.BatchNorm1d(fc_size[i+1]))
                fcs.append(nn.ReLU())
        self.fcs = nn.Sequential(*fcs)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
        # whether to give back the features in the forward pass (e.g. for RL master)
        self.return_features = return_features

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        features = x.view(x.size(0), -1)
        x = self.fcs(features)

        return x if not self.return_features else (x, features)

class ResNetBranch(nn.Module):

    def __init__(self, block, layers, block_depth=[64, 128, 256, 512], fc_hidden=[512, 512], 
                input_dim=3, dim_action=8, num_skills=1, return_features=False, **kwargs):
        self.inplanes = 64
        self.dim_action = dim_action
        self.num_skills = num_skills
        super(ResNetBranch, self).__init__()

        self.conv1 = nn.Conv2d(input_dim, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, block_depth[0], layers[0])
        self.layer2 = self._make_layer(block, block_depth[1], layers[1], stride=2)
        self.layer3 = self._make_layer(block, block_depth[2], layers[2], stride=2)
        self.layer4 = self._make_layer(block, block_depth[3], layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)

        # fully connected layers on top of features
        fc_size = [block_depth[3]*block.expansion]+fc_hidden+[dim_action]
        fcs = []
        # one fc head per skill 
        for k in range(num_skills):
            fcs.append([])
            for i in range(len(fc_size)-1):
                fcs[-1].append(nn.Linear(fc_size[i], fc_size[i+1]))
                if i < len(fc_size)-2:
                    # fcs[-1].append(nn.BatchNorm1d(fc_size[i+1]))
                    fcs[-1].append(nn.ReLU())
            fcs[-1] = nn.Sequential(*fcs[-1])
        self.fcs = nn.Sequential(*fcs)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # whether to give back the features in the forward pass (e.g. for RL master)
        self.return_features = return_features

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        features = x.view(x.size(0), -1)

        x_tot = None
        for fc_skill in self.fcs: 
            x_skill = fc_skill(features)
            if x_tot is None:
                x_tot = x_skill
            else:
                x_tot = torch.cat((x_tot, x_skill), 1) 

        return x_tot if not self.return_features else (x_tot, features)

class ResNetB4Branch(nn.Module):

    def __init__(self, block, layers, block_depth=[64, 128, 256, 512], fc_hidden=[512, 512], 
                input_dim=3, dim_action=8, num_skills=1, return_features=False, **kwargs):
        inplanes = 64
        self.dim_action = dim_action
        self.num_skills = num_skills
        super(ResNetB4Branch, self).__init__()

        self.conv1 = nn.Conv2d(input_dim, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1, inplanes = self._make_layer(block, block_depth[0], layers[0], inplanes=inplanes)
        self.layer2, inplanes = self._make_layer(block, block_depth[1], layers[1], stride=2, inplanes=inplanes)
        self.layer3, inplanes = self._make_layer(block, block_depth[2], layers[2], stride=2, inplanes=inplanes)
        self.layer4_mult = nn.Sequential(
            *[self._make_layer(block, block_depth[3], layers[3], stride=2, inplanes=inplanes)[0] for _ in range(num_skills)])
        self.avgpool = nn.AvgPool2d(7, stride=1)

        # fully connected layers on top of features
        fc_size = [block_depth[3]*block.expansion]+fc_hidden+[dim_action]
        fcs = []
        # one fc head per skill 
        for _ in range(num_skills):
            fcs.append([])
            for i in range(len(fc_size)-1):
                fcs[-1].append(nn.Linear(fc_size[i], fc_size[i+1]))
                if i < len(fc_size)-2:
                    fcs[-1].append(nn.BatchNorm1d(fc_size[i+1]))
                    fcs[-1].append(nn.ReLU())
            fcs[-1] = nn.Sequential(*fcs[-1])
        self.fcs = nn.Sequential(*fcs)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # whether to give back the features in the forward pass (e.g. for RL master)
        self.return_features = return_features

    def _make_layer(self, block, planes, blocks, stride=1, inplanes=64):
        downsample = None
        if stride != 1 or inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(inplanes, planes, stride, downsample))
        inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(inplanes, planes))

        return nn.Sequential(*layers), inplanes

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)

        '''x = self.avgpool(x)
        features = x.view(x.size(0), -1)'''

        x_tot = None
        for layer4, fc in zip(self.layer4_mult, self.fcs):
            x_branch = layer4(x)
            x_branch = self.avgpool(x_branch)
            x_branch = x_branch.view(x_branch.size(0), -1)
            x_branch = fc(x_branch)
            if x_tot is None:
                x_tot = x_branch
            else:
                x_tot = torch.cat((x_tot, x_branch), 1)

        return x_tot # if not self.return_features else (x_tot, features)

class ResNetGoal(nn.Module):

    def __init__(self, block, layers, block_depth=[64, 128, 256, 512], input_dim=3, num_classes=1000):
        self.inplanes = 64
        super(ResNetGoal, self).__init__()

        dim_feat = block_depth[3]

        # goal network
        self.conv1 = nn.Conv2d(input_dim, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, block_depth[0], layers[0])
        self.layer2 = self._make_layer(block, block_depth[1], layers[1], stride=2)
        self.layer3 = self._make_layer(block, block_depth[2], layers[2], stride=2)
        self.layer4 = self._make_layer(block, block_depth[3], layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        self.fc1 = nn.Linear(dim_feat*2*block.expansion, dim_feat*2)
        self.bn_fc1 = nn.BatchNorm1d(dim_feat*2)
        self.fc2 = nn.Linear(dim_feat*2, num_classes)
        self.relu = nn.ReLU(inplace=True)

        '''# option network, from option and current feature to goal
        self.opt_fc1 = nn.Linear(dim_feat*block.expansion+3, dim_feat)
        self.opt_bn1 = nn.BatchNorm1d(dim_feat)
        self.opt_fc2 = nn.Linear(dim_feat*block.expansion, dim_feat)
        self.opt_bn2 = nn.BatchNorm1d(dim_feat)
        self.opt_fc3 = nn.Linear(dim_feat*block.expansion, dim_feat)'''

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        self.goal_feat = None
        self.goal_option = None

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward_conv(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)

        return x

    '''# x : resnet feature
    # y : option vector, one-hot encoding
    def shallow_net(self, x, y):
        z = torch.cat((x, y), 1)
        z = self.opt_fc1(z)+x
        z = self.opt_bn1(z)
        z = self.relu(z)
        z = self.opt_fc2(z)+z
        z = self.opt_bn2(z)
        z = self.relu(z)
        z = self.opt_fc3(z)+z

        return z'''

    def set_goal(self, x):
        self.goal_feat = self.forward_conv(x)

    '''def set_option(self, option):
        self.goal_option = option

    def set_goal_from_option(self, x, option):
        feat = self.forward_conv(x)
        self.goal_option = option
        self.goal_feat = self.shallow_net(feat, option)'''
    
    def forward(self, x):
        if self.goal_feat is None:
            raise ValueError('Goal feature was not defined. Call set_goal first.')
        
        current_feat = self.forward_conv(x)
        '''current_feat_input = current_feat.detach()
        transfer_feat = self.shallow_net(current_feat_input, self.goal_option)'''
    
        # concatenate feature of current observation and goal feature
        z = torch.cat((current_feat, self.goal_feat), 1)
        
        z = self.fc1(z)
        z = self.bn_fc1(z)
        z = self.relu(z)
        z = self.fc2(z)


        # returns a detached version of the goal that do not requires grad as it will be the objective of the shallow network
        # return z, transfer_feat, self.goal_feat.detach()
        return z# , torch.zeros(1), self.goal_feat.detach()


class ResNetDANN(nn.Module):

    def __init__(self, block, layers, input_dim=3, num_classes=1000):
        self.inplanes = 64
        super(ResNetDANN, self).__init__()
        self.conv1 = nn.Conv2d(input_dim, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7, stride=1)
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        self.d_fc1 = nn.Linear(512 * block.expansion, 512)
        self.d_bn1 = nn.BatchNorm1d(512)
        self.d_fc2 = nn.Linear(512, 2)
        # self.d_fc1 = nn.Linear(512 * block.expansion, 64)
        # self.d_bn1 = nn.BatchNorm1d(64)
        # self.d_fc2 = nn.Linear(64, 2)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x, alpha):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)

        # Control
        x_ctrl = self.fc(x)

        # Domain Adaptation
        x_dmn = FReverseLayer.apply(x, alpha)
        x_dmn = self.d_fc1(x_dmn)
        x_dmn = self.d_bn1(x_dmn)
        x_dmn = F.relu(x_dmn)
        x_dmn = self.d_fc2(x_dmn)

        return x_ctrl, x_dmn

def small_resnet10(pretrained=False, input_dim=3, **kwargs):
    model = ResNetFC(BasicBlock, [1, 1, 1, 1], [32, 64, 128, 256], [], input_dim, **kwargs)
    return model

def small_resnet10_fc(pretrained=False, input_dim=3, **kwargs):
    model = ResNetFC(BasicBlock, [1, 1, 1, 1], [32, 64, 128, 256], [256], input_dim, **kwargs)
    return model

def skills_small_resnet10(pretrained=False, input_dim=3, dim_action=8, num_skills=1, **kwargs):
    model = ResNetBranch(BasicBlock, [1, 1, 1, 1], [32, 64, 128, 256], [256, 256], input_dim, dim_action, num_skills, **kwargs)
    return model

def skills_small_resnet10_2(pretrained=False, input_dim=3, dim_action=8, num_skills=1, **kwargs):
    model = ResNetBranch(BasicBlock, [1, 1, 1, 1], [32, 64, 128, 256], [64,64], input_dim, dim_action, num_skills, **kwargs)
    return model

def resnet10(pretrained=False, input_dim=3, **kwargs):
    model = ResNetFC(BasicBlock, [1, 1, 1, 1], [64, 128, 256, 512], [], input_dim, **kwargs)
    return model

def resnet18(pretrained=False, input_dim=3, **kwargs):
    model = ResNetFC(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], [], input_dim, **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet18']))
    return model

def standard_resnet18(pretrained=False, input_dim=3, **kwargs):
    model = ResNet(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], **kwargs)
    if pretrained:
        state_dict = model_zoo.load_url(model_urls['resnet18'])
        # remove the last layer
        state_dict.pop('fc.weight')
        state_dict.pop('fc.bias')
        state_dict['fc.weight'] = model.state_dict()['fc.weight']
        state_dict['fc.bias'] = model.state_dict()['fc.bias']
        model.load_state_dict(state_dict)
    return model

def resnet18_featbranch(pretrained=False, input_dim=3, dim_action=8, num_skills=1, **kwargs):
    model = ResNetBranch(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], [64, 64], input_dim, dim_action, num_skills, **kwargs)
    return model

def resnet18_b4branch(pretrained=False, input_dim=3, dim_action=8, num_skills=1, **kwargs):
    model = ResNetB4Branch(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], [64], input_dim, dim_action, num_skills, **kwargs)
    return model

def resnet18_fc64(pretrained=False, input_dim=3, **kwargs):
    model = ResNetFC(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], [64], input_dim, **kwargs)
    return model

def skills_resnet18(pretrained=False, input_dim=3, dim_action=8, num_skills=1, **kwargs):
    model = ResNetBranch(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], [512, 512], input_dim, dim_action, num_skills, **kwargs)
    return model

def resnet18_fc(pretrained=False, input_dim=3, **kwargs):
    model = ResNetFC(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], [512], input_dim, **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet18']))
    return model

def goal_small_resnet10(pretrained=False, input_dim=3, **kwargs):
    model = ResNetGoal(BasicBlock, [1, 1, 1, 1], [32, 64, 128, 256], input_dim, **kwargs)
    return model

def goal_resnet18(pretrained=False, input_dim=3, **kwargs):
    model = ResNetGoal(BasicBlock, [2, 2, 2, 2], [64, 128, 256, 512], input_dim, **kwargs)
    return model

def dann_resnet18(pretrained=False, input_dim=3, **kwargs):
    model = ResNetDANN(BasicBlock, [2, 2, 2, 2], input_dim, **kwargs)
    return model


'''def hiearchical_resnet18(heads_size = [4], **kwargs):
    """Constructs a ResNet-18 model.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNetHiearchical(BasicBlock, [2, 2, 2, 2], heads_size, **kwargs)
    return model'''


def resnet34(pretrained=False, **kwargs):
    model = ResNetFC(BasicBlock, [3, 4, 6, 3], [64, 128, 256, 512], [], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet34']))
    return model


def resnet50(pretrained=False, **kwargs):
    model = ResNetFC(Bottleneck, [3, 4, 6, 3], [], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet50']))
    return model


def resnet101(pretrained=False, **kwargs):
    model = ResNetFC(Bottleneck, [3, 4, 23, 3], [64, 128, 256, 512], [], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet101']))
    return model


def resnet152(pretrained=False, **kwargs):
    model = ResNetFC(Bottleneck, [3, 8, 36, 3], [64, 128, 256, 512], [], **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['resnet152']))
    return model

