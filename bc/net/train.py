import click
from tqdm import tqdm
from argparse import Namespace
from sacred import Experiment
from sacred.observers import FileStorageObserver


from ..config import train_ingredient
from .utils import make_loader, make_net, write_info, get_train_loss, compute_loss,\
    append_losses, save_losses, run_evaluation
from . import log

ex = Experiment('train', ingredients=[train_ingredient])
# ex.observers.append(FileStorageObserver.create('/sequoia/data1/rstrudel/experiments/bc'))

@ex.capture
def train(train_loader, eval_loader, net, dir_net, train_loss, optimizer,
          scheduler, starting_epoch,
          train):
          # epochs, eval_interval, eval_proportion, dir_net, archi):
    print('Starting training...')
    for epoch in range(starting_epoch, starting_epoch + train['epochs']):
        scheduler.step()
        print('Epoch {}'.format(epoch))
        for i, batch in tqdm(enumerate(train_loader)):
            optimizer.zero_grad()
            batch_losses = compute_loss(batch, net)
            optimizer.step()
            append_losses(train_loss, batch_losses)
            if i % 500 == 0:
                save_losses(train_loss, dir_net)
            log.train(train_loss, epoch, i, len(train_loader))

        if epoch % train['eval_interval'] == 0 and train['eval_proportion'] is not None:
            eval_loss_dict = run_evaluation(net, eval_loader, epoch, train_loss.keys())
            log.eval(eval_loss_dict, epoch, len(train_loader))

        if epoch % 2 == 0 and epoch > 0:
            net.save(path='{}/{}_{}.pth'.format(dir_net, net.archi, epoch))
        net.save('{}/{}_current.pth'.format(dir_net, net.archi))

@ex.automain
def main():
    train_loader, eval_loader, statistics = make_loader()
    net, optimizer, scheduler, starting_epoch, dir_net = make_net()
    write_info(statistics)
    train_loss = get_train_loss()
    log.init_writers(dir_net)
    train(train_loader, eval_loader, net, dir_net, train_loss, optimizer,
          scheduler, starting_epoch)

# @click.option('--timestamp', type=str, default=None)  # Alex running tool compatibility
# @click.option('--time_training', type=str, default=None)  # Alex running tool compatibility
# @click.option('--seed', type=str, default=None)  # Alex running tool compatibility
