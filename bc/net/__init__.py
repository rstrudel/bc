from .zoo.base import Network
from .zoo.hierarchical import NetworkHierarchical
from .zoo.da import NetworkDA
from .zoo.regression import NetworkReg
from .zoo.goal import NetworkGoal
from .zoo.skills import NetworkSkills
# from . import utils
